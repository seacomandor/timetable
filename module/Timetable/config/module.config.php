<?php
return array(
    'doctrine' => [
        'driver' => [
            'timetable_entities' => [
                'class' => Doctrine\ORM\Mapping\Driver\AnnotationDriver::class,
                'paths' => [__DIR__ . '/../src/Timetable/Entities'],
            ],
            'orm_default' => [
                'drivers' => [
                    'Timetable\Entities' => 'timetable_entities',
                ],
            ],
        ],
    ],

    'controllers' => [
      'factories' => [
        \Timetable\Controller\TodayController::class => \Timetable\Factory\TodayController::class,
      ],
    ],

    'router' => [
        'routes' => [
            'today' => [
                'type' => 'segment',
                'options' => [
                    'route' => '/today[/][:action]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        'controller' => \Timetable\Controller\TodayController::class,
                        'action' => 'index',
                    ],
                ],

            ],
        ],
    ],

    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
);