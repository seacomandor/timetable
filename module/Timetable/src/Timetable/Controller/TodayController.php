<?php
/**
 * Created by PhpStorm.
 * User: comandor
 * Date: 12.02.17
 * Time: 20:39
 */

namespace Timetable\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Doctrine\Common\Collections\Criteria;
use Zend\View\Model\JsonModel;

use Timetable\Entities;

class TodayController extends AbstractActionController
{
    private $_em;

    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        $this->_em = $em;
    }

    public function indexAction()
    {
        if($this->getRequest()->isGet()){
            $startDateP = $this->params()->fromQuery('start_period');
            $endDateP = $this->params()->fromQuery('end_period');
            if(isset($startDateP) && isset($endDateP)){
                $startDate = new \DateTime($startDateP);
                $endDate = new \DateTime($endDateP);
            }
        }

        if(!isset($startDate) && !isset($endDate)) {
            $startDate = new \DateTime();
            $endDate = new \DateTime('+7 days');
        }

        $timeTable = $this->_em->getRepository(Entities\TimeTable::class)->matching(
            Criteria::create()->where(Criteria::expr()->gte('_departureDate', $startDate))->andWhere(
                Criteria::expr()->lte('_arrivalDate', $endDate)
            )->orderBy(['_departureDate' => 'DESC', '_arrivalDate' => 'DESC'])
        );
        
        $timeTableArr = [];
        /** @var Entities\TimeTable $item */
        foreach ($timeTable as $item){
            $timeTableArr[] = [
                'courier' => $item->getCourier()->getName(),
                'region' => $item->getToRegion()->getName(),
                'departure_date' => $item->getDepartureDate()->format('d.m.Y'),
                'arrival_date' => $item->getDepartureDate()->add(
                  new \DateInterval("P{$item->getToRegion()->getTimeFromMSK()}D")
                )->format('d.m.Y'),
            ];
        }
        return ['timetable' => $timeTableArr, 'startDate' => $startDate->format('Y-m-d'),
            'endDate' => $endDate->format('Y-m-d')];
    }

    public function getCouriersAction()
    {
        $this->getRequest();
        $departure = $this->params()->fromQuery('depart_date');
        $arrival = $this->params()->fromQuery('arrival_date');
        $dt = $at = new \DateTime();
        if(isset($departure) && isset($arrival)) {
            $dt = new \DateTime($departure);
            $at = new \DateTime($arrival);
        }
        $couriers = $this->_em->getRepository(Entities\Courier::class)->findAll();
        $couriers2View = [];
        /** @var Entities\Courier $courier */
        foreach ($couriers as $courier){
            $inTrip = $courier->getTimeTable()->matching(Criteria::create()->where(
                Criteria::expr()->andX(
                    Criteria::expr()->lte('_departureDate', $dt),
                    Criteria::expr()->gte('_arrivalDate', $dt)
                )
            )->orWhere(
                Criteria::expr()->andX(
                    Criteria::expr()->lte('_departureDate', $at),
                    Criteria::expr()->gte('_arrivalDate', $at)
                )
            ));
            if(count($inTrip) > 0) continue;
            
            $couriers2View[$courier->getId()] = $courier->getName();
        }
        return new JsonModel(['couriers' => $couriers2View]);
    }

    public function getRegionsAction()
    {
        $regions = $this->_em->getRepository(Entities\Region::class)->findAll();
        $regions2View = [];
        /** @var Entities\Region $region */
        foreach ($regions as $region){
            $regions2View[$region->getId()] = [
                'name' => $region->getName(),
                'time2Msk' => $region->getTimeFromMSK(),
            ];
        }

        return new JsonModel(['regions' => $regions2View]);
    }

    public function saveAction()
    {
        if($this->getRequest()->isPost()){
            $post = $this->getRequest()->getPost();
            /** @var Entities\Courier $courier */
            $courier = $this->_em->getRepository(Entities\Courier::class)->find($post['courier']);
            /** @var Entities\Region $region */
            $region = $this->_em->getRepository(Entities\Region::class)->find($post['region']);
            if($region instanceof Entities\Region && $courier instanceof Entities\Courier) {
                $timeTable = new Entities\TimeTable();
                $timeTable->setCourier($courier)
                    ->setToRegion($region)
                    ->setDepartureDate(new \DateTime($post['departure_date']))
                    ->setArrivalDate((new \DateTime($post['departure_date']))->add(
                        new \DateInterval("P{$region->getTimeFromMSK()}D")
                    ));
                $this->_em->persist($timeTable);
                $this->_em->flush();

                return new JsonModel(['success']);
            }
        }
        return new JsonModel(['fail']);
    }
}