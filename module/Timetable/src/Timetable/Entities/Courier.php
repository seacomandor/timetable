<?php
/**
 * Created by PhpStorm.
 * User: comandor
 * Date: 12.02.17
 * Time: 19:16
 */

namespace Timetable\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Courier
 * @package Timetable\Entities
 *
 * @ORM\Entity
 * @ORM\Table(
 *     name="couriers"
 * )
 */
class Courier
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(name="id", type="bigint", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $_id;

    /**
     * @var string
     *
     * @ORM\Column(name="fio", type="string", length=255, nullable=false)
     */
    protected $_name;


    /**
     * @var TimeTable
     * @ORM\OneToMany(targetEntity="TimeTable", mappedBy="_courier")
     *
     */
    protected $_timeTable;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @return \Doctrine\ORM\Persisters\Collection\OneToManyPersister
     */
    public function getTimeTable()
    {
        return $this->_timeTable;
    }

}