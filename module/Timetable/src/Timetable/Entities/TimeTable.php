<?php
/**
 * Created by PhpStorm.
 * User: comandor
 * Date: 12.02.17
 * Time: 20:08
 */

namespace Timetable\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class TimeTable
 * @package Timetable\Entities
 *
 * @ORM\Entity
 * @ORM\Table(name="timetable",
 *     uniqueConstraints = {
 *      @ORM\UniqueConstraint(name="courier_region_date", columns={"courier_id", "to_region_id", "departure_date"})
 * })
 *
 */
class TimeTable
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(name="id", type="bigint", options={"unsigned"=true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $_id;

    /**
     * @var Courier
     * @ORM\ManyToOne(targetEntity="Courier")
     * @ORM\JoinColumn(name="courier_id", referencedColumnName="id")
     */
    protected $_courier;

    /**
     * @var Region
     * @ORM\ManyToOne(targetEntity="Region")
     * @ORM\JoinColumn(name="to_region_id", referencedColumnName="id")
     */
    protected $_toRegion;

    /**
     * @var \DateTime
     * @ORM\Column(name="departure_date", type="date")
     */
    protected $_departureDate;

    /**
     * @var \DateTime
     * @ORM\Column(name="arrival_date", type="date")
     */
    protected $_arrivalDate;

    /**
     * @return Courier
     */
    public function getCourier()
    {
        return $this->_courier;
    }

    /**
     * @return Region
     */
    public function getToRegion()
    {
        return $this->_toRegion;
    }

    /**
     * @return \DateTime
     */
    public function getDepartureDate()
    {
        return $this->_departureDate;
    }

    /**
     * @param Courier $courier
     */
    public function setCourier($courier)
    {
        $this->_courier = $courier;
        return $this;
    }

    /**
     * @param Region $toRegion
     */
    public function setToRegion($toRegion)
    {
        $this->_toRegion = $toRegion;
        return $this;
    }

    /**
     * @param \DateTime $departureDate
     */
    public function setDepartureDate($departureDate)
    {
        $this->_departureDate = $departureDate;
        return $this;
    }

    /**
     * @param \DateTime $arrivalDate
     */
    public function setArrivalDate($arrival)
    {
        $this->_arrivalDate =  $arrival;
    }


}