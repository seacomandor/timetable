<?php
/**
 * Created by PhpStorm.
 * User: comandor
 * Date: 12.02.17
 * Time: 19:21
 */

namespace Timetable\Entities;


use Doctrine\ORM\Mapping as ORM;

/**
 * Class Region
 * @package Timetable\Entities
 *
 * @ORM\Entity
 * @ORM\Table(
 *     name="regions"
 *)
 *
 */
class Region
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(name="id", type="bigint", options={"unsigned": true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $_id;

    /**
     * @var string
     * @ORM\Column(name="region_name", type="string", length=255, nullable=false)
     */
    protected $_name;

    /**
     * @var int
     * @ORM\Column(name="time_from_msk", type="integer", options={"unsigned"=true}, nullable=false)
     */
    protected $_timeFromMSK;

    /**
     * @var TimeTable
     *
     * @ORM\OneToMany(targetEntity="TimeTable",mappedBy="_toRegion")
     */
    protected $_timeTable;
    /**
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @return int
     */
    public function getTimeFromMSK()
    {
        return $this->_timeFromMSK;
    }

    /**
     * @param int $timeFromMSK
     */
    public function setTimeFromMSK($timeFromMSK)
    {
        $this->_timeFromMSK = $timeFromMSK;
    }

    /**
     * @return TimeTable
     */
    public function getTimeTable()
    {
        return $this->_timeTable;
    }

    
}