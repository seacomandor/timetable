<?php
/**
 * Created by PhpStorm.
 * User: comandor
 * Date: 12.02.17
 * Time: 20:54
 */

namespace Timetable\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

use Timetable\Controller as Controllers;


class TodayController implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $entityManager = $serviceLocator->getServiceLocator()->get(\Doctrine\ORM\EntityManager::class);
        return new Controllers\TodayController($entityManager);
    }
}