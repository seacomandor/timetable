/**
 * Created by comandor on 12.02.17.
 */
$( document ).ready(function () {
    $(".dp1").datetimepicker({pickTime: false, format: 'YYYY-MM-DD', minDate: new Date() });
    $(".dp2").datetimepicker({pickTime: false, format: 'YYYY-MM-DD' });

    $(".dp1").on('dp.change', function (e) {
        getCouriers();
    });



    $("#myModal").on('show.bs.modal', function (e) {
        getCouriers();

        $.get("/today/getRegions", function (data) {
            $("select#region").html('');
            $.map(data['regions'], function (value, key) {
                $("select#region").append(
                    '<option value="' + key + '" data-content="' + $(value).attr('time2Msk') + '">'+ $(value).attr('name') + "</option>"
                );
            });
        });
    });

    $("select#region").change(function () {
        getCouriers();
    });
    
    
    $("#save_trip_button").click(function () {
        $.post('/today/save', $(this).parents("div.modal").find("form.form-horizontal").serialize())
            .done(function (data) {
                if(data[0] == 'success'){
                    window.location.replace(window.location);
                } else {
                    alert("Опс, ошибка!");
                }
            })
            .fail(function () {
                alert('Опс вообще ошибка!');
            })
    })
});

function getCouriers() {
    var depart = $("#depart_date").val();
    var url = "/today/getCouriers";
    if(depart && depart.length != 0){
        url += "?depart_date=" + depart + "&arrival_date=" + getArrivalDate();
    }
    $("select#courier").html('');
    $.get(url, function (data) {
        $.map(data['couriers'], function (value, key) {
            $("select#courier").append(
                '<option value="' + key + '">'+ value + "</option>"
            );
        });

    });
}


function getArrivalDate()
{
    var depart_date = $("#depart_date").val();
    if(depart_date != ''){
        depart_date = moment(depart_date, 'YYYY.MM.DD');
        var option = $("select#region option:selected");
        var days = $(option).data('content');
        var arrival_date = depart_date.add(days, 'days');
        $("#tripinfo").html(
            "Дата прибытия в регион " + $(option).text() + " - " + arrival_date.format('DD.MM.YYYY')
        );
        return arrival_date.format('YYYY-MM-DD');
    }
}