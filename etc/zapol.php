<?php
/*Заполнить данные по поездкам с июня 2015 года  */


$start = strtotime('2015-06-01');
$end = time();

$mysqli = new \Mysqli(
    'localhost',
    'timetable',
    'timetable',
    'timetable',
    3306
);
$couriers = [];
$query = "SELECT
            id
          FROM
            couriers";
$result = $mysqli->query($query, MYSQLI_USE_RESULT);
while($row = $result->fetch_assoc()){
    $couriers[] = $row['id'];
}


$query = "SELECT
            id, 
            time_from_msk
          FROM
            regions";
$result = $mysqli->query($query);
$regions = [];
while($row = $result->fetch_assoc()){
    $regions[$row['id']] = $row['time_from_msk'];
}

$couriersTimes = [];


while($start < $end){
    foreach ($couriers as $courier){
        if(!isset($couriersTimes[$courier])){
            $couriersTimes[$courier] = [];
        }


        if(!isset($couriersTimes[$courier][$start])){
            $region = rand(1,9);
            $startTrip = $start;
            $endTrip = tripTime($startTrip, $regions[$region]);
            while($startTrip <= $endTrip){
                $couriersTimes[$courier][$startTrip] = $region;
                $startTrip += 24 * 60 *60;
            }
        }
    }
    $start += 24 *60 *60;
}


$couriersDates = [];

foreach ($couriersTimes as $courier => $times){
    $curRegion = $startTime = $endTime = null;
    foreach ($times as $time => $region){
        if(!isset($startTime)){
            $startTime = $time;
            $endTime = tripTime($startTime, $regions[$region]);
            $curRegion = $region;
        }
        if($time == $endTime){
            $depart = date('Y-m-d', $startTime);
            $arrival = date('Y-m-d', $endTime);
            $couriersDates[] = "({$courier},{$curRegion},'{$depart}','{$arrival}')";
            $curRegion = $startTime = $endTime = null;
        }
    }
}

$mysqli->query('TRUNCATE timetable');
$query = "INSERT INTO timetable(courier_id,to_region_id,departure_date,arrival_date)
          VALUES " . implode(',', $couriersDates);
$mysqli->query($query);

function tripTime($start, $duration)
{
    return $start + $duration * 24 * 60 * 60;
}